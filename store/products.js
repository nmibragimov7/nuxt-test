export default {
  state: () => ({
    products: [],
    isLoading: false,
    error: null
  }),
  mutations: {
    setProducts(state, payload) {
      state.isLoading = false
      state.products = payload
      state.error = null
    },
    startLoading(state) {
      state.isLoading = true
      state.products = []
      state.error = null
    },
    endLoading(state) {
      state.isLoading = false
    },
    setError(state, error) {
      state.error = error
    }
  },
  actions: {
    async fetchProducts(context) {
      context.commit('startLoading')
      let products = []
      await this.$axios.$get('https://angulartest-1f024-default-rtdb.firebaseio.com/products.json').then((response) => {
        Object.keys(response).forEach((key) => {
          products.push({
            id: key,
            userId: response[key].userId,
            title: response[key].title,
            body: response[key].body
          })
        })
        context.commit('endLoading')
        context.commit('setProducts', products)
      }).catch(error => {
        context.commit('endLoading')
        context.commit('setError', error)
      })
    }
  },
  getters: {
    products: (state) => { return state.products},
    isLoading: (state) => { return state.isLoading},
    error: (state) => { return state.error}
  }
}
